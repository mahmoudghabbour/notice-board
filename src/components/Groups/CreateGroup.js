import React, { Component } from "react";
import { connect } from "react-redux";
import { createGroup } from "../../store/action/GroupAction";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { Redirect } from "react-router-dom";

const styles = (theme) => ({
  FormControl: { width: "540px" },
});

var uuid = require("uuid");
class CreateGroup extends Component {
  state = {
    id: "",
    nameGroup: "",
    description: "",
    ownerGroup: "",
    ownerGroupId: "",
  };
  constructor(props) {
    console.log("con", props);
    super(props);
  }
  componentDidMount(props) {
    axios
      .get("/api/signedin/")
      .then((response) => {
        console.log("sh", response);
        this.setState({
          ownerGroupId: response.data.uid,
          ownerGroup: response.data.name,
          id: uuid.v4(),
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  // On file select (from the pop up)
  onFileChange = (event) => {
    // Update the state
    this.setState({ selectedFile: event.target.files[0] });
    console.log(event.target.files[0]);
  };

  // On file upload (click the upload button)
  onFileUpload = () => {
    // Create an object of formData
    const formData = new FormData();

    // Update the formData object
    formData.append(
      "myFile",
      this.state.selectedFile,
      this.state.selectedFile.name
    );

    // Details of the uploaded file
    console.log(this.state.selectedFile);

    // Request made to the backend api
    // Send formData object
    axios.post("api/uploadfile", formData);
  };
  handleSubmit = (e) => {
    console.log(e);
    e.preventDefault();
    this.props.createGroup(this.state);
    console.log("this.state", this.state);
    axios
      .post("http://localhost:5000/api/groups", this.state)
      .then((response) => {
        console.log("response", response);
        this.props.handleClose();
      })
      .catch((error) => {
        console.log(error);
        this.props.handleClose();
      });
  };
  fileData = () => {
    if (this.state.selectedFile) {
      return (
        <div>
          <h2>File Details:</h2>
          <p>File Name: {this.state.selectedFile.name}</p>
          <p>File Type: {this.state.selectedFile.type}</p>
          <p>
            Last Modified:{" "}
            {this.state.selectedFile.lastModifiedDate.toDateString()}
          </p>
        </div>
      );
    } else {
      return (
        <div>
          <br />
          <h4>Choose before Pressing the Upload button</h4>
        </div>
      );
    }
  };
  render() {
    console.log("this.props", this.props);
    const { classes, redirect } = this.props;
    console.log(redirect);
    if (redirect == true) return <Redirect to="/" />;
    return (
      <div className={classes.FormControl}>
        <DialogTitle id="form-dialog-title">Create Group</DialogTitle>
        <DialogContent>
          <DialogContentText>Please fill out the form below</DialogContentText>
          <div className="input-field">
            <label className="left" htmlFor="text">
              Group Name
            </label>
            <input
              className="form-control"
              type="text"
              id="nameGroup"
              onChange={this.handleChange}
            />
          </div>
          <div className="input-field">
            <label htmlFor="content">Description</label>
            <input
              id="description"
              type="text"
              className="form-control"
              onChange={this.handleChange}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            style={{ backgroundColor: "#ec407a", color: "white" }}
            onClick={this.handleSubmit}
          >
            Create
          </Button>
        </DialogActions>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  console.log("state", state);
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createGroup: (group) => dispatch(createGroup(group)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(CreateGroup);
