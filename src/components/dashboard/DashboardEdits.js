import React, { Component } from "react";
import AllEdits from "../Groups/notes/AllEdits";
import { connect } from "react-redux";
import axios from "axios";

class DashboardEdits extends Component {
  constructor() {
    super();
    this.state = {
      notes: [],
    };
  }
  componentDidMount() {
    axios
      .get("/api/notes/")
      .then((response) => {
        console.log(response);
        for (let i = 0; i < response.data.length; i++) {
          if (response.data[i][0].id == this.props.match.params.noteId) {
            this.setState({ notes: response.data[i] });
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    console.log("this.state", this.state);
    const { notes } = this.state;
    console.log("notesvv", notes);
    const id = this.props.match.params.id;
    return (
      <div className="dashboard container">
        <div className="row">
          <div className="col s12 m6 note">
            {this.state.notes &&
              this.state.notes.map((note) => {
                return <AllEdits note={note} id={id} />;
              })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps)(DashboardEdits);
