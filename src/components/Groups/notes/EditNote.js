import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import { createNote } from "../../../store/action/NoteAction";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { Redirect } from "react-router-dom";
var uuid = require("uuid");
const type = "edit";
const styles = (theme) => ({
  FormControl: { width: "540px" },
});
class EditNote extends Component {
  state = {
    id: "",
    title: "",
    content: "",
    createdTime: "",
    ownerId: "",
    groupId: "",
    name: "",
    groupName: "",
    file: {
      fileName: "",
      typeFile: "",
      sizeFile: "",
    },
  };
  constructor(props) {
    console.log("con", props);
    super(props);
    this.state = {
      groupId: props.groupId,
      id: props.noteId,
    };
  }
  componentDidMount(props) {
    axios
      .get("/api/notes/")
      .then((response) => {
        console.log("notes", response);
        for (let i = 0; i < response.data.length; i++) {
          if (response.data[i][0].id == this.props.noteId) {
            if (response.data[i].length > 1) {
              this.setState({
                title: response.data[i][response.data[i].length - 1].title,
                content: response.data[i][response.data[i].length - 1].content,
                name: response.data[i][response.data[i].length - 1].name,
                groupName:
                  response.data[i][response.data[i].length - 1].groupName,
                fileName:
                  response.data[i][response.data[i].length - 1].fileName,
                typeFile:
                  response.data[i][response.data[i].length - 1].typeFile,
                sizeFile:
                  response.data[i][response.data[i].length - 1].sizeFile,
              });
            } else {
              this.setState({
                title: response.data[i][0].title,
                content: response.data[i][0].content,
                name: response.data[i][0].name,
                groupName: response.data[i][0].groupName,
                fileName: response.data[i][0].fileName,
                typeFile: response.data[i][0].typeFile,
                sizeFile: response.data[i][0].sizeFile,
              });
            }
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
    axios
      .get("/api/signedin/")
      .then((response) => {
        console.log("sh", response);
        this.setState({
          ownerId: response.data.uid,
          createdTime: new Date(),
          //  id: uuid.v4(),
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  onFileChange = (event) => {
    // Update the state
    console.log(event.target.files[0]);
    this.setState({
      fileName: event.target.files[0].name,
      typeFile: event.target.files[0].type,
      sizeFile: event.target.files[0].size,
    });
  };
  onFileUpload = () => {
    // Create an object of formData
    const formData = new FormData();

    // Update the formData object
    formData.append(
      "myFile",
      this.state.selectedFile,
      this.state.selectedFile.name
    );

    // Details of the uploaded file
    console.log(this.state.selectedFile);

    // Request made to the backend api
    // Send formData object
    axios.post("api/uploadfile", formData);
  };
  fileData = () => {
    if (this.state.fileName) {
      return (
        <div>
          <h2>File Details:</h2>
          <p>File Name: {this.state.fileName}</p>
          <p>File Type: {this.state.typeFile}</p>
          {/* <p>
            Last Modified:{" "}
            {this.state.selectedFile.lastModifiedDate.toDateString()}
         </p>*/}
        </div>
      );
    } else {
      return (
        <div>
          {/* <br />
          <h4>Choose before Pressing the Upload button</h4>*/}
        </div>
      );
    }
  };
  handleSubmit = (e) => {
    console.log(e);
    e.preventDefault();
    var data = this.state;
    console.log("data", data);

    this.props.createNote(this.state);
    console.log("this.state", this.state);
    axios
      .post("http://localhost:5000/api/notes", this.state)
      .then((response) => {
        console.log("response", response);
        axios
          .post("http://localhost:5000/api/notifications", { data, type })
          .then((response) => {
            console.log("response", response);
            this.props.handleClose();
          })
          .catch((error) => {
            console.log(error);
            this.props.handleClose();
          });
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    console.log("this.state", this.state);
    const { classes, redirect } = this.props;
    console.log("this.props", this.props);
    if (redirect == true)
      return (
        <Redirect
          to={"/groupdetails/" + this.props.groupId + "/dashboardnotes/"}
        />
      );
    return (
      <div className={classes.FormControl}>
        <DialogTitle id="form-dialog-title">Edit Note</DialogTitle>
        <DialogContent>
          <DialogContentText>Please fill out the form below</DialogContentText>
          <div className="input-field">
            <label className="left" htmlFor="text">
              Note Title
            </label>
            <input
              className="form-control"
              type="text"
              id="title"
              onChange={this.handleChange}
              defaultValue={this.state.title}
            />
          </div>
          <div className="input-field">
            <label htmlFor="content">Content</label>
            <input
              id="content"
              type="text"
              className="form-control"
              onChange={this.handleChange}
              defaultValue={this.state.content}
            />
          </div>
          <div>
            <input type="file" onChange={this.onFileChange} />
          </div>
          {this.fileData()}
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            style={{ backgroundColor: "#ec407a", color: "white" }}
            onClick={this.handleSubmit}
          >
            Update
          </Button>
        </DialogActions>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  console.log("state", state);
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createNote: (note) => dispatch(createNote(note)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(EditNote);
