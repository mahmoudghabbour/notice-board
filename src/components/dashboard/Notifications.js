import React, { Component } from "react";
import axios from "axios";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
var id = 0;
var type = "delete";
class Notifications extends Component {
  constructor() {
    super();
    this.state = {
      notifications: [],
      users: [],
      id: "",
      name: "",
    };
  }
  componentDidMount() {
    axios
      .get("/api/signedin/")
      .then((response) => {
        console.log("vv", response);
        this.setState({ id: response.data.uid, name: response.data.name });
      })
      .catch((error) => {
        console.log(error);
      });
    axios
      .get("/api/notifications/")
      .then((response) => {
        console.log(response);
        this.setState({ notifications: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
    axios
      .get("/api/users/")
      .then((response) => {
        console.log(response);
        this.setState({ users: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  handleSubmit = (e, groupId, name, userId) => {
    console.log("bla", e, groupId, name, userId);
    axios
      .post("http://localhost:5000/api/users", { userId, name, groupId })
      .then((response) => {
        console.log("response", response);
      })
      .catch((error) => {
        console.log(error);
      });
    axios
      .post("http://localhost:5000/api/notifications", {
        userId,
        name,
        groupId,
        type,
      })
      .then((response) => {
        console.log("response", response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    const { notifications } = this.state;
    console.log("notifications", notifications);
    console.log("id", this.state.id);
    console.log("this.state.users", this.state.users);
    return (
      <div>
        <Typography variant="h3" gutterBottom>
          Notifications
        </Typography>

        {notifications &&
          notifications.map((notification) => {
            console.log("map", notification);
            if (notification.type == "join") {
              if (notification.ownerId == this.state.id) {
                console.log("gg", notification);
                return (
                  <div className="notes-list section">
                    <div className="card-content grey-text text-darken-3">
                      <div className="card z-depth-0 notes-summary">
                        <span className="card-title">{notification.type}</span>
                        <p>
                          {notification.name} wants to join to your group{" "}
                          {notification.groupName}
                        </p>
                        <p>Time: {notification.time}</p>
                        <div style={{ marginBottom: "20px" }}>
                          <div className="d-flex justify-content-center">
                            <Button
                              variant="contained"
                              onClick={(e) =>
                                this.handleSubmit(
                                  e,
                                  notification.groupId,
                                  notification.name,
                                  notification.userId
                                )
                              }
                            >
                              Accept
                            </Button>
                            <Button variant="contained">Decline</Button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              }
            } else if (notification.type == "post") {
              return (
                <div>
                  {this.state.users &&
                    this.state.users.map((user) => {
                      console.log(user.groupId, notification.data.groupId);
                      console.log(user.userId, this.state.id);
                      if (
                        user.groupId == notification.data.groupId &&
                        user.userId == this.state.id
                      ) {
                        return (
                          <div className="notes-list section">
                            <div className="card-content grey-text text-darken-3">
                              <div className="card z-depth-0 notes-summary">
                                <span className="card-title">
                                  {notification.type}
                                </span>
                                <p>
                                  {notification.data.name} has posted in{" "}
                                  {notification.data.groupName}
                                </p>
                                <p>title: {notification.data.title}</p>
                                <p>Time: {notification.data.createdTime}</p>
                              </div>
                            </div>
                          </div>
                        );
                      }
                    })}
                </div>
              );
            } else if (notification.type == "edit") {
              return (
                <div>
                  {this.state.users &&
                    this.state.users.map((user) => {
                      console.log(user.groupId, notification.data.groupId);
                      console.log(user.userId, this.state.id);
                      if (
                        user.groupId == notification.data.groupId &&
                        user.userId == this.state.id
                      ) {
                        return (
                          <div className="notes-list section">
                            <div className="card-content grey-text text-darken-3">
                              <div className="card z-depth-0 notes-summary">
                                <span className="card-title">
                                  {notification.type}
                                </span>
                                <p>
                                  {notification.data.name} has edited in{" "}
                                  {notification.data.groupName}
                                </p>
                                <p>title: {notification.data.title}</p>
                                <p>Time: {notification.data.createdTime}</p>
                              </div>
                            </div>
                          </div>
                        );
                      }
                    })}
                </div>
              );
            }
          })}
      </div>
    );
  }
}

export default Notifications;
