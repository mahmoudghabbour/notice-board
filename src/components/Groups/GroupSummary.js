import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

class GroupSummary extends Component {
  state = {
    id: "",
  };
  componentDidMount() {
    axios
      .get("/api/signedin/")
      .then((response) => {
        console.log(response);
        this.setState({ id: response.data.uid });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    const { group, handleOpen, handleOpenSignIn } = this.props;
    console.log("this.state.id", this.state.id);
    console.log("note", group.id);
    return (
      <div className="card z-depth-0 notes-summary">
        {this.state.id != null ? (
          <Link to={"/groupdetails/" + group.id}>
            <div className="card-content grey-text text-darken-3">
              <span className="card-title">{group.nameGroup}</span>
              <p>Description: {group.description}</p>
              <p className="grey-text">3rd September, 2am</p>
            </div>
          </Link>
        ) : (
          <div
            onClick={handleOpenSignIn}
            className="card-content grey-text text-darken-3"
          >
            <span className="card-title">{group.nameGroup}</span>
            <p>Description: {group.description}</p>
            <p className="grey-text">3rd September, 2am</p>
          </div>
        )}
      </div>
    );
  }
}

export default GroupSummary;
