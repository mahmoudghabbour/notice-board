import React from "react";
import ReactDOM from "react-dom";
import Button from "@material-ui/core/Button";
import "./App.css";
import Navbar from "./components/layout/Navbar";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Dashboard from "./components/dashboard/Dashboard";
import GroupDetails from "./components/Groups/GroupDetails";
import SignIn from "./components/auth/SignIn";
import SignUp from "./components/auth/SignUp";
import CreateGroup from "./components/Groups/CreateGroup";
import CreateNote from "./components/Groups/notes/CreateNote";
import EditNote from "./components/Groups/notes/EditNote";
import DashboardNotes from "./components/dashboard/DashboardNotes";
import NoteDetails from "./components/Groups/notes/NoteDetails";
import AllEdits from "./components/Groups/notes/AllEdits";
import DashboardEdits from "./components/dashboard/DashboardEdits";
import DashboardUsers from "./components/dashboard/DashboardUsers";
function App(props) {
  console.log("sss", props);
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Switch>
          <Route
            path="/groupdetails/:id/dashboardnotes/:noteId/alledits"
            component={DashboardEdits}
          />
          <Route
            path="/groupdetails/:id/dashboardnotes/:noteId/editnote"
            component={EditNote}
          />
          <Route
            path="/groupdetails/:id/dashboardnotes/:noteId"
            component={NoteDetails}
          />
          <Route
            path="/groupdetails/:id/dashboardnotes"
            component={DashboardNotes}
          />
          <Route
            path="/groupdetails/:id/dashboardusers"
            component={DashboardUsers}
          />
          <Route path="/groupdetails/:id/createnote" component={CreateNote} />
          <Route path="/groupdetails/:id" component={GroupDetails} />
          <Route path="/creategroup" component={CreateGroup} />
          <Route path="/signup" component={SignUp} />
          <Route path="/signin" component={SignIn} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/" component={Dashboard} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
