import React, { Component } from "react";
import NotesList from "../Groups/notes/NotesList";
import { connect } from "react-redux";
import axios from "axios";
class DashboardNotes extends Component {
  constructor() {
    super();
    this.state = {
      notes: [],
    };
  }
  componentDidMount() {
    axios
      .get("/api/notes/")
      .then((response) => {
        console.log(response);
        this.setState({ notes: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    console.log("this.props", this.props);
    const { notes } = this.state;
    console.log("notesvv", notes);
    const id = this.props.match.params.id;
    return (
      <div className="dashboard container">
        <div className="row">
          <div className="col s12 m6 note">
            <NotesList notes={notes} id={id} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps)(DashboardNotes);
