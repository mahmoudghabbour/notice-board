import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Button } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import SignIn from "../auth/SignIn";
import SignUp from "../auth/SignUp";
import { Redirect } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
const styles = (theme) => ({
  FormControl: { width: "540px" },
  buttons: {
    "&:hover": { cursor: "pointer" },
  },
});
class SignedOutLinks extends Component {
  state = { open: false, redirect: false, openSignUp: false };
  handleOpen = () => {
    this.setState({
      open: true,
    });
  };
  handleClose = () => {
    this.setState({
      open: false,
      redirect: true,
    });
  };
  handleOpenSignUp = () => {
    this.setState({
      openSignUp: true,
    });
  };
  handleCloseSignUp = () => {
    this.setState({
      openSignUp: false,
      redirect: true,
    });
  };
  render() {
    const { classes } = this.props;
    if (this.state.redirect == true) return <Redirect to="/" />;
    return (
      <ul className="right">
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <SignIn
            handleClose={this.handleClose}
            redirect={this.state.redirect}
          />
        </Dialog>
        <Dialog
          open={this.state.openSignUp}
          onClose={this.handleCloseSignUp}
          aria-labelledby="form-dialog-title"
        >
          <SignUp
            handleCloseSignUp={this.handleCloseSignUp}
            redirect={this.state.redirect}
          />
        </Dialog>
        
        <li onClick={this.handleOpenSignUp} className={classes.buttons}>
          Sign Up
        </li>
        <li>{"   "}</li>
        <li
          onClick={this.handleOpen}
          style={{ marginLeft: "20px" }}
          className={classes.buttons}
        >
          Log In
        </li>
      </ul>
    );
  }
}

export default withStyles(styles)(SignedOutLinks);
