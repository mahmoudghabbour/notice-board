import React from "react";
import NotesSummary from "./NotesSummary";
const NotesList = ({ notes, id }) => {
  console.log(notes);
  console.log("id", id);
  return (
    <div className="notes-list section note">
      {notes &&
        notes.map((note) => {
         // console.log(note);
          if (note[0].groupId == id) {
            if (note[0].length == 1) {
              console.log(note[0]);
              return <NotesSummary note={note[0]} id={id} key={note.id} />;
            } else {
              console.log(note[0]);
              return (
                <NotesSummary
                  note={note[note.length - 1]}
                  id={id}
                  key={note.id}
                />
              );
            }
          }
        })}
    </div>
  );
};

export default NotesList;
