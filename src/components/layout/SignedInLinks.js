import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { signOut } from "../../store/action/AuthAction";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import CreateGroup from "../Groups/CreateGroup";
class SignedInLinks extends Component {
  state = { open: false, redirect: false };
  handleOpen = () => {
    this.setState({
      open: true,
    });
  };
  handleClose = () => {
    this.setState({
      open: false,
      redirect: true,
    });
  };
  render() {
    console.log(this.props);
    const { name } = this.props;
    console.log(name[0]);
    return (
      <ul className="right">
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <CreateGroup
            handleClose={this.handleClose}
            redirect={this.state.redirect}
          />
        </Dialog>
        <li onClick={this.handleOpen}>
          <NavLink to="/">New Group</NavLink>
        </li>
        <li onClick={this.props.signOut}>
          <NavLink to="/">Log Out</NavLink>
        </li>
        <li>
          <NavLink to="/" className="btn btn-floating pink lighten-1">
            {name[0]}
          </NavLink>
        </li>
      </ul>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut()),
  };
};

export default connect(null, mapDispatchToProps)(SignedInLinks);
