import React from "react";
import { Link } from "react-router-dom";
const AllEdits = ({ note, id }) => {
  console.log("note", note.id);

  return (
    <div className="card z-depth-0 notes-summary">
      <Link to={"/groupdetails/" + id + "/dashboardnotes/" + note.id}>
        <div className="card-content grey-text text-darken-3">
          <span className="card-title">{note.title}</span>
          <p>Content: {note.content}</p>
        </div>
      </Link>
    </div>
    
  );
};

export default AllEdits;
