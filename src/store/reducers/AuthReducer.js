const initState = {
  auth: null,
};

const AuthReducer = (state = initState, action) => {
  switch (action.type) {
    case "SIGNED_IN_SUCCESSFULY":
      console.log("SignedIn", action);
      state.auth = "Successful";
      return state;
    case "SIGNED_UP_SUCCESSFULY":
      console.log("SignedUp", action);
      return action.data.id;
    case "ERROR_SIGNED_IN":
      console.log("error sign in");
      break;
    case "SIGNOUT_SUCCESS":
      console.log("sign out");
      return null;
  }
  return state;
};

export default AuthReducer;
