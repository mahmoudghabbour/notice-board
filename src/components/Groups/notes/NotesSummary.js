import React from "react";
import { Link } from "react-router-dom";
const NotesSummary = ({ note, id }) => {
  console.log("note", note.id);

  return (
    <div className="card z-depth-0 notes-summary">
      <Link to={"/groupdetails/" + id + "/dashboardnotes/" + note.id}>
        <div className="card-content grey-text text-darken-3">
          <span className="card-title">{note.title}</span>
          <p>Content: {note.content}</p>

          {note.fileName != null ? (
            <div>
              <h4>File Details:</h4>
              <p>File Name: {note.fileName}</p>
              <p>File Type: {note.typeFile}</p>
              <p>File Size: {note.sizeFile}</p>
            </div>
          ) : null}
        </div>
      </Link>
    </div>
  );
};

export default NotesSummary;
