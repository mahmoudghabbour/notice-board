import React, { Component } from "react";
import ReactDOM from "react-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import SignedInLinks from "./SignedInLinks";
import SignedOutLinks from "./SignedOutLinks";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
class Navbar extends Component {
  state = {
    id: "",
  };
  componentDidMount() {
    axios
      .get("/api/signedin/")
      .then((response) => {
        console.log("sh", response);
        this.setState({ id: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    console.log("props", this.state.id.uid);
    console.log(this.state);
    return (
      <nav className="nav-wrapper grey darken-3">
        <div className="container">
          <div className="left">
            <div className="d-flex">
              <div>
                <Link to="/" className="brand-logo">
                  <img className="logoImage" src="/img/logo-1.png" />
                </Link>
              </div>
              <div style={{ marginLeft: "60px" }}>
                <Link to="/" className="brand-logo">
                  Notice Board
                </Link>
              </div>
            </div>
          </div>
          {this.state.id.uid != null ? (
            <SignedInLinks props={this.props} name={this.state.id.name} />
          ) : (
            <SignedOutLinks />
          )}
        </div>
      </nav>
    );
  }
}

const mapStateToProps = (state, props) => {
  console.log(state);
  console.log(props);

  return {
    auth: state.auth,
  };
};

export default connect(mapStateToProps, null)(Navbar);
