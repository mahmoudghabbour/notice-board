import React from "react";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Dialog from "@material-ui/core/Dialog";
import UsersSummary from "./UsersSummary";

const style = {
  margin: 0,
  top: "auto",
  right: 20,
  bottom: 20,
  left: "auto",
  position: "fixed",
  backgroundColor: "#ec407a",
  color: "white",
};
const UsersList = ({ users, id }) => {
  return (
    <div className="notes-list section">
      {users &&
        users.map((user) => {
          if (user.groupId == id)
            return <UsersSummary user={user} id={id} key={user.id} />;
        })}
    </div>
  );
};

export default UsersList;
