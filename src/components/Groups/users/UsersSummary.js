import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { Button } from "@material-ui/core";
import axios from "axios";

var type = "delete";
class UsersSummary extends Component {
  constructor(props) {
    super();
    console.log(props);
    this.state = {
      groupId: props.user.groupId,
      userId: props.user.userId,
      redirect: false,
    };
  }
  handleDelete = () => {
    const { groupId, userId } = this.state;
    axios
      .post("http://localhost:5000/api/users", { groupId, userId, type })
      .then((response) => {
        console.log("response", response);
        this.setState({
          redirect: true,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    const { user, id } = this.props;
    if (this.state.redirect == true)
      return <Redirect to={"/groupdetails/" + id} />;
    return (
      <div className="card z-depth-0 notes-summary">
        <div className="card-content grey-text text-darken-3">
          <span className="card-title">{user.name}</span>
          <p>
            <Button variant="contained" onClick={this.handleDelete}>
              Delete
            </Button>
          </p>
        </div>
      </div>
    );
  }
}

export default UsersSummary;
