import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { signUp } from "../../store/action/AuthAction";
import { Redirect } from "react-router-dom";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
const styles = (theme) => ({
  FormControl: { width: "540px" },
});
var uuid = require("uuid");
class SignUp extends Component {
  state = {
    email: "",
    password: "",
    name: "",
    id: uuid.v4(),
  };
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state);
    this.props.signUp(this.state);
    axios
      .post("http://localhost:5000/api/auth", this.state)
      .then((response) => {
        console.log("response", response);
        this.props.handleCloseSignUp();
      })
      .catch((error) => {
        console.log(error);
        this.props.handleCloseSignUp();
      });
  };
  render() {
    const { classes } = this.props;
    if (this.props.redirect == true) return <Redirect to="/" />;
    return (
      <div className={classes.FormControl}>
        <DialogTitle id="form-dialog-title">Sign Up</DialogTitle>
        <DialogContent>
          <div className="input-field">
            <label htmlFor="lastName"> Name</label>
            <input type="text" id="name" onChange={this.handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="email">Email</label>
            <input type="email" id="email" onChange={this.handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="password">Password</label>
            <input type="password" id="password" onChange={this.handleChange} />
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            style={{ backgroundColor: "#ec407a", color: "white" }}
            onClick={this.handleSubmit}
          >
            Sign Up
          </Button>
        </DialogActions>
      </div>
    );
  }
}

const mapDispatchToProps = (disatch) => {
  return {
    signUp: (data) => disatch(signUp(data)),
  };
};

export default compose(
  withStyles(styles),
  connect(null, mapDispatchToProps)
)(SignUp);
