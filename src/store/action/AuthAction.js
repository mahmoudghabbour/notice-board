import axios from "axios";

export const signIn = (data, auth) => {
  return (dispatch, getState) => {
    console.log(auth);
    console.log("data", data);
    var isFalse = false;
    for (let i = 0; i < auth.length; i++) {
      if (auth[i].email == data.email && auth[i].password == data.password) {
        {
          console.log(auth[i]);
          const uid = auth[i].id;
          const name = auth[i].name;
          console.log("auth[i]", auth[i]);
          console.log("uid", uid);
          axios
            .post("http://localhost:5000/api/signedin", { uid, name })
            .then((response) => {
              console.log("response", response);
            })
            .catch((error) => {
              console.log(error);
            });
          isFalse = true;
          dispatch({
            type: "SIGNED_IN_SUCCESSFULY",
            data,
            uid,
          });
        }
      }
    }
    if (isFalse == false) {
      dispatch({
        type: "ERROR_SIGNED_IN",
      });
    }
  };
};

export const signUp = (data) => {
  return (dispatch, getState) => {
    console.log("data", data);
    const uid = data.id;
    const name = data.name;
    axios
      .post("http://localhost:5000/api/signedin", { uid, name })
      .then((response) => {
        console.log("response", response);
      })
      .catch((error) => {
        console.log(error);
      });
    dispatch({
      type: "SIGNED_UP_SUCCESSFULY",
      data,
    });
  };
};

export const signOut = () => {
  return (dispatch, getState) => {
    const uid = null;
    axios
      .post("http://localhost:5000/api/signedin", { uid })
      .then((response) => {
        console.log("response", response);
      })
      .catch((error) => {
        console.log(error);
      });
    dispatch({ type: "SIGNOUT_SUCCESS" });
  };
};
