import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import axios from "axios";
import EditNote from "./EditNote";
import Dialog from "@material-ui/core/Dialog";

const type = "delete";

var userId,
  ownerId,
  time,
  name,
  groupId = 0;
class NoteDetails extends Component {
  state = {};
  constructor() {
    super();
    this.state = {
      notes: [],
      idNow: "",
      isOpen: false,
      redirect: false,
    };
  }
  componentDidMount(props) {
    console.log("tk", this.props);
    axios
      .get("/api/notes/")
      .then((response) => {
        console.log("fk", response);
        for (let i = 0; i < response.data.length; i++) {
          if (response.data[i][0].id === this.props.match.params.noteId) {
            if (response.data[i].length == 1) {
              this.setState({
                notes: response.data[i][0],
              });
            } else {
              this.setState({
                notes: response.data[i][response.data[i].length - 1],
              });
            }
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
    axios
      .get("/api/signedin/")
      .then((response) => {
        this.setState({
          idNow: response.data.uid,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  handleDelete = () => {
    console.log(this.props);
    const groupId = this.props.match.params.id;
    const noteId = this.props.match.params.noteId;
    axios
      .post("http://localhost:5000/api/notes", { groupId, noteId, type })
      .then((response) => {
        console.log("response", response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  handleOpen = () => {
    this.setState({
      isOpen: true,
    });
  };
  handleClose = () => {
    this.setState({
      isOpen: false,
      redirect: true,
    });
  };
  render() {
    console.log("this.state", this.state);
    console.log(this.props);
    const id = this.props.match.params.id;
    const noteId = this.props.match.params.noteId;
    return (
      <div className="container section note-details">
        <Dialog
          open={this.state.isOpen}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <EditNote
            groupId={id}
            noteId={noteId}
            handleClose={this.handleClose}
            redirect={this.state.redirect}
          />
        </Dialog>
        <div className="card z-depth-0">
          <div className="card-content">
            <span className="card-title">
              Note Name: {this.state.notes.title}{" "}
            </span>
            <p>Content: {this.state.notes.content}</p>
            {this.state.notes.fileName != null ? (
              <div>
                <h4>File Details:</h4>
                <p>File Name: {this.state.notes.fileName}</p>
                <p>File Type: {this.state.notes.typeFile}</p>
                <p>File Size: {this.state.notes.sizeFile}</p>
              </div>
            ) : null}
          </div>
          <div className="card-action gret lighten-4 grey-text">
            {this.state.idNow == this.state.notes.ownerId ? (
              <Button variant="contained" onClick={this.handleOpen}>
                Edit
              </Button>
            ) : null}
            <Link
              to={
                "/groupdetails/" +
                id +
                "/dashboardnotes/" +
                noteId +
                "/alledits"
              }
            >
              <Button variant="contained">Show Edits</Button>
            </Link>
            {this.state.idNow == this.state.notes.ownerId ? (
              <Button variant="contained" onClick={this.handleDelete}>
                Delete
              </Button>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state, props) => {
  console.log("state", state);
  console.log("props", props);
  return {};
};
export default connect(mapStateToProps, null)(NoteDetails);
