import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import axios from "axios";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import CreateNote from "./notes/CreateNote";
import Dialog from "@material-ui/core/Dialog";
const style = {
  margin: 0,
  top: "auto",
  right: 20,
  bottom: 20,
  left: "auto",
  position: "fixed",
  backgroundColor: "#ec407a",
  color: "white",
};
const type = "join";
var userId,
  ownerId,
  time,
  name,
  groupId,
  groupName = 0;
class GroupDetails extends Component {
  state = {};
  constructor() {
    super();
    this.state = {
      userIdNow: "",
      isOpen: false,
      redirect: false,
      groups: [],
      users: [],
      notification: {
        userId: "",
        ownerId: "",
        time: "",
        name: "",
        type: "",
      },
    };
  }
  componentDidMount(props) {
    console.log("tk", this.props);
    axios
      .get("/api/groups/")
      .then((response) => {
        console.log("response", response);
        this.setState({ groups: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
    axios
      .get("/api/users/")
      .then((response) => {
        console.log("fk", response);
        this.setState({
          users: response.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
    axios
      .get("/api/signedin/")
      .then((response) => {
        console.log("ff", response);
        name = response.data.name;
        userId = response.data.uid;
        this.setState({
          userIdNow: response.data.uid,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  handleSubmitNotification = (e) => {
    e.preventDefault();
    console.log("tk", this.props);

    console.log(e);

    console.log("this.state", this.state);
    time = new Date();
    console.log("what");
    console.log(time, userId, ownerId, name);

    this.setState({
      userId: userId,
      time: time,
      ownerId: ownerId,
      name: name,
      type: type,
    });
    console.log("groupName", groupName);
    /*  console.log(time, userId, ownerId, name);
    var notification = {};
    notification.push(type, time, userId, ownerId, name);*/
    axios
      .post("http://localhost:5000/api/notifications", {
        userId,
        time,
        ownerId,
        name,
        type,
        groupId,
        groupName,
      })
      .then((response) => {
        console.log("response", response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  handleOpen = () => {
    this.setState({
      isOpen: true,
    });
  };
  handleClose = () => {
    this.setState({
      isOpen: false,
      redirect: true,
    });
  };
  render() {
    groupId = this.props.match.params.id;
    const id = this.props.match.params.id;
    var group = [];

    for (let i = 0; i < this.state.groups.length; i++) {
      if (this.state.groups[i].id == id) {
        group = this.state.groups[i];
        groupName = this.state.groups[i].nameGroup;
        ownerId = this.state.groups[i].ownerGroupId;
      }
    }
    var isFalse = false;

    for (let i = 0; i < this.state.users.length; i++) {
      console.log(this.state.users[i].userId);
      console.log(userId);
      if (this.state.users[i].userId == userId) {
        console.log("apo");
        isFalse = true;
      }
    }
    console.log(this.state.userIdNow);
    console.log("isFalse", isFalse);
    if (isFalse === true || ownerId == this.state.userIdNow) {
      return (
        <div className="container section note-details">
          <Dialog
            open={this.state.isOpen}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
          >
            <CreateNote
              groupId={groupId}
              handleClose={this.handleClose}
              redirect={this.state.redirect}
            />
          </Dialog>
          <div className="card z-depth-0">
            <div className="card-content">
              <span className="card-title">Group Name: {group.nameGroup}</span>
              <p>Description: {group.description}</p>
            </div>
            <div className="card-action gret lighten-4 grey-text">
              <div>Owner Name: {group.ownerGroup}</div>

              <Link to={"/groupdetails/" + id + "/dashboardnotes"}>
                <Button variant="contained">Show notes</Button>
              </Link>
              {this.state.userIdNow === ownerId ? (
                <Link to={"/groupdetails/" + id + "/dashboardusers"}>
                  <Button variant="contained">Show users</Button>
                </Link>
              ) : null}
              {this.state.userIdNow === ownerId ? (
                <Button variant="contained" onClick={this.handleOpen}>
                  Create Note
                </Button>
              ) : null}
              {this.state.userIdNow === ownerId ? (
                <Fab aria-label="add" style={style} onClick={this.handleOpen}>
                  <AddIcon />
                </Fab>
              ) : null}
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="container section note-details">
          <div className="card z-depth-0">
            <div className="card-content">
              <span className="card-title">Group Name: {group.nameGroup}</span>
              <p>Description: {group.description}</p>
            </div>
            <div className="card-action gret lighten-4 grey-text">
              <div>Owner Name: {group.ownerGroup}</div>
              <div>
                <Button
                  variant="contained"
                  onClick={this.handleSubmitNotification}
                >
                  Join
                </Button>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}
const mapStateToProps = (state, props) => {
  console.log("state", state);
  console.log("props", props);
  return {};
};
export default connect(mapStateToProps, null)(GroupDetails);
