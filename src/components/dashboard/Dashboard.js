import React, { Component } from "react";
import { GroupList } from "../Groups/GroupList";
import { connect } from "react-redux";
import axios from "axios";
import Notifications from "./Notifications";
import Dialog from "@material-ui/core/Dialog";
import CreateGroup from "../Groups/CreateGroup";
import { Redirect } from "react-router-dom";
import SignIn from "../auth/SignIn";
class Dashboard extends Component {
  constructor() {
    super();
    this.state = {
      groups: [],
      open: false,
      openSignIn: false,
      redirect: false,
      id: "",
    };
  }
  componentDidMount() {
    axios
      .get("/api/groups/")
      .then((response) => {
        console.log(response);
        this.setState({ groups: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
    axios
      .get("/api/signedin/")
      .then((response) => {
        console.log(response);
        this.setState({ id: response.data.uid });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  handleOpen = () => {
    this.setState({
      open: true,
    });
  };
  handleClose = () => {
    this.setState({
      open: false,
      redirect: true,
    });
  };
  handleOpenSignIn = () => {
    this.setState({
      openSignIn: true,
    });
  };
  handleCloseSignIn = () => {
    this.setState({
      openSignIn: false,
      redirect: true,
    });
  };
  render() {
    console.log("this.props", this.props);
    console.log("this.state", this.state);
    const { groups } = this.state;
    console.log("notes", groups);
    if (this.state.redirect == true) return <Redirect to="/" />;
    return (
      <div className="dashboard container">
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <CreateGroup
            handleClose={this.handleClose}
            redirect={this.state.redirect}
          />
        </Dialog>
        <Dialog
          open={this.state.openSignIn}
          onClose={this.handleCloseSignIn}
          aria-labelledby="form-dialog-title"
        >
          <SignIn
            handleClose={this.handleCloseSignIn}
            redirect={this.state.redirect}
          />
        </Dialog>
        <div className="row">
          <div className="col s12 m6">
            <GroupList
              groups={groups}
              open={this.state.open}
              redirect={this.state.redirect}
              handleOpen={this.handleOpen}
              handleClose={this.handleClose}
              handleOpenSignIn={this.handleOpenSignIn}
              id={this.state.id}
            />
          </div>
          <div className="col s12 m5 offset-m1">
            <Notifications />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps)(Dashboard);
