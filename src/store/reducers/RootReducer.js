import GroupReducer from "./GroupReducer";
import AuthReducer from "./AuthReducer";
import { combineReducers } from "redux";

const RootReducer = combineReducers({
  auth: AuthReducer,
  group: GroupReducer,
});

export default RootReducer;
