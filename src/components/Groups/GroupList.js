import React from "react";
import GroupSummary from "./GroupSummary";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import CreateGroup from "./CreateGroup";
import Dialog from "@material-ui/core/Dialog";

const style = {
  margin: 0,
  top: "auto",
  right: 20,
  bottom: 20,
  left: "auto",
  position: "fixed",
  backgroundColor: "#ec407a",
  color: "white",
};
export const GroupList = ({
  groups,
  open,
  redirect,
  handleOpen,
  handleClose,
  handleOpenSignIn,
  id,
}) => {
  console.log(groups);

  return (
    <div className="notes-list section">
      {groups &&
        groups.map((group) => {
          return (
            <GroupSummary
              group={group}
              handleOpen={handleOpen}
              handleOpenSignIn={handleOpenSignIn}
              key={group.id}
            />
          );
        })}
      {id ? (
        <Fab aria-label="add" style={style} onClick={handleOpen}>
          <AddIcon />
        </Fab>
      ) : null}
    </div>
  );
};
