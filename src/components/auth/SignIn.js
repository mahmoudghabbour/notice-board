import React, { Component } from "react";
import { connect } from "react-redux";
import { signIn } from "../../store/action/AuthAction";
import axios from "axios";
import { Redirect } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
const styles = (theme) => ({
  FormControl: { width: "540px" },
});
var auth = [];
class SignIn extends Component {
  state = {
    email: "",
    password: "",
  };

  componentDidMount() {
    axios
      .get("/api/auth/")
      .then((response) => {
        console.log("response", response);
        auth.push(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  handleSubmit = (e) => {
    console.log(e);
    e.preventDefault();
    console.log(this.state);
    this.props.signIn(this.state);
    this.props.handleClose();
  };
  render() {
    console.log("auth", auth[0]);
    console.log("this.props", this.props);
    const { classes } = this.props;
    if (this.props.redirect == true) return <Redirect to="/" />;
    return (
      <div className={classes.FormControl}>
        <DialogTitle id="form-dialog-title">Sign In</DialogTitle>
        <DialogContent>
          <div className="input-field">
            <label htmlFor="email">Email</label>
            <input type="email" id="email" onChange={this.handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              className="form-control"
              onChange={this.handleChange}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            style={{ backgroundColor: "#ec407a", color: "white" }}
            onClick={this.handleSubmit}
          >
            Sign In
          </Button>
        </DialogActions>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  console.log("state", state);
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (disatch) => {
  return {
    signIn: (data) => disatch(signIn(data, auth[0])),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(SignIn);
