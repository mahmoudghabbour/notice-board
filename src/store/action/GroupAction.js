export const createGroup = (group) => {
  return (dispatch, getState) => {
    dispatch({
      type: "CREATE_GROUP",
      group,
    });
  };
};
