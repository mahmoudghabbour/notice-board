import React, { Component } from "react";
import UsersList from "../Groups/users/UsersList";
import { connect } from "react-redux";
import axios from "axios";
class DashboardUsers extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
    };
  }
  componentDidMount() {
    axios
      .get("/api/users/")
      .then((response) => {
        console.log(response);
        this.setState({ users: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    console.log("this.props", this.props);
    const { users } = this.state;
    console.log("notesvv", users);
    const id = this.props.match.params.id;
    return (
      <div className="dashboard container">
        <div className="row">
          <div className="col s12 m6 note">
            <UsersList users={users} id={id} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps)(DashboardUsers);
